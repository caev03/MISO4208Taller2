describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select-input').find('input').type('Nicolas Escobar',{force: true})
        cy.wait(2000)
        cy.get('.Select-menu-outer').contains('Nicolas Escobar Velasquez - matematicas')
    })
})