describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select-input').find('input').type('Mario Linares Vasquez',{force: true})
        cy.wait(1500)
        cy.get('.Select-menu-outer').find('div[class="Select-option is-focused"]').click()
        cy.get('.materias').find('input[name="ISIS1206L"]').check()
        cy.get('.boxElementFirstChild').should("not.contain", "Lab. Estructuras Datos")
    })
})