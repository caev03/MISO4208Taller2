import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  searchHeroInInput(searchHero: string){
    element(by.tagName('input')).sendKeys(searchHero);
    return element.all(by.className('search-result'));
  }

  deleteHero(){
    element(by.className('delete')).click();
  }

  editHero(searchHero: string, editHero: string){
    element(by.tagName('input')).sendKeys(searchHero);
    element(by.className('search-result')).click();
    element(by.tagName('input')).sendKeys(editHero)
    element(by.buttonText('Save')).click();
    element(by.tagName('input')).sendKeys(editHero);
    return element.all(by.className('search-result'));
  }
  
  visitHeroFromSearch(hero: string){
    element(by.tagName('input')).sendKeys(hero);
    element(by.className('search-result')).click();
    return element.all(by.tagName('hero-detail'))
  }

  visitHeroFromDashboard(){
    element(by.css('.module.hero')).click();
    return element.all(by.tagName('hero-detail'))
  }

  visitHeroFromHeroesList(){
    element(by.className('heroes')).element(by.tagName('li')).click();
    element(by.buttonText('View Details')).click();
    return element.all(by.tagName('hero-detail'))
  }
}
