var assert = require('assert');
describe('los estudiantes login', function() {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.waitForVisible('button=Cerrar', 10000);
        browser.pause(500)
        browser.click('button=Cerrar');
        browser.click('button=Ingresar');
        browser.waitForVisible('.cajaLogIn', 10000);
        browser.pause(500)

        var cajaLogIn = browser.element('.cajaLogIn');
        browser.waitForVisible('input[name="correo"]', 10000);
        browser.pause(500)
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail@example.com');
        browser.waitForVisible('input[name="password"]', 10000);
        browser.pause(500)
        var passwordInput = cajaLogIn.element('input[name="password"]');
        

        passwordInput.click();
        passwordInput.keys('1234');
        browser.waitForVisible('button=Ingresar', 10000);
        browser.pause(500)
        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 10000);
        browser.pause(500)

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });

    it('should visit los estudiantes and fails registering a already create user', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Ingresar');
        browser.waitForVisible('.cajaSignUp', 10000);
        browser.pause(500)

        var cajaSignUp = browser.element('.cajaSignUp');
        browser.waitForVisible('input[name="correo"]', 10000);
        browser.pause(500)
        var mailInput = cajaSignUp.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('ca.escobar2434@uniandes.edu.co');
        var nameInput = cajaSignUp.element('input[name="nombre"]')
        nameInput.click();
        nameInput.keys('Camilo');
        var nameInput = cajaSignUp.element('input[name="apellido"]')
        nameInput.click();
        nameInput.keys('Escobar');
        var uniSelect = cajaSignUp.element('select[name="idUniversidad"]')
        uniSelect.selectByAttribute('value', 'universidad-de-los-andes')
        var idDepartamento = cajaSignUp.element('select[name="idDepartamento"]')
        idDepartamento.selectByAttribute('value', '3')
        var passwordInput = cajaSignUp.element('input[name="password"]')
        passwordInput.click();
        passwordInput.keys('Camilo');
        var passwordInput = cajaSignUp.element('input[name="acepta"]')
        passwordInput.click();

        browser.waitForVisible('button=Registrarse', 10000);
        browser.pause(500)
        
        cajaSignUp.element('button=Registrarse').click()
        browser.waitForVisible('.text-muted.lead', 10000);
        browser.pause(500)

        var alertText = browser.element('.text-muted.lead').element('div').getText();
        expect(alertText).toBe("Error: Ya existe un usuario registrado con el correo 'ca.escobar2434@uniandes.edu.co'");
    });

    it('should visit los estudiantes and Search for teacher', function () {
        browser.url('https://losestudiantes.co');
        var teacherInput = browser.element('.Select-input').element('input');
        teacherInput.keys('Nicolas Escobar');
        browser.waitForVisible('.Select-option.is-focused', 10000);
        browser.pause(500)
        var alertText = browser.element('.Select-option.is-focused').getText();
        expect(alertText).toBe("Nicolas Escobar Velasquez - matematicas");
    });
});